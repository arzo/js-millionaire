js-millionnaire
===

Are you into startups, but you cannot get your first million?
This script automates this for you.

How does it work?
====

This script works hard to find out what bank account are you using and then tries to earn as much as possible.

OK, OK, ... so where's the trick?
====

Unfortunately..., money earned this way are very volatile... they are gone after reloading the page.

Some serious stuff
===

*DISCLAIMER|WARNING|WHATEVER*

NEVER EVER NEVER run this script on your real bank account
since running this script causes potential risk of accessing
third parties to your account.

Author of this script takes no responsibility for stupid behaviour
when using this script in a stupid way.

Portfolio
===

So far, this script allows increasing rate of earning money on following bank accounts. 

![PKO Bank Polski](ipko-pl.png)