/***

DISCLAIMER|WARNING|WHATEVER

NEVER EVER NEVER run this script on your real bank account, 
since running this script causes potential risk of accessing 
third parties to your account. 

Author of this script takes no responsibility for stupid behaviour 
when using this script in a stupid way. 

INSTRUCTION

Go to http://demo.ipko.pl/ (note that PKO does not serve demo via HTTPS :'( ), 
login using demo credentials
right click on background and run (in .*Chrom[ium|e]) _Inspector_ or _Inspect Element_ (in .*Firefox)
execute script (note that on Firefox you need to write _allow pasting_ first)

Notes from the author

tricky was separating digits in a smart way, but regexp \d{3} splitting 
and then reversing groups of digits did the job. 

(c) arjamizo 2016
***/

location.href.match(/pko/) && 
$('span:contains('+document.querySelector('.h5+.h1').textContent.trim().match(/...[,.]?.. /)[0]+')')
    .text(((Math.random()*1e9|0)/100+" PLN ").replace(".",",")
	  .replace(/([0-9 ]+)/, function(e){console.log(e); return e.replace(/\d{3}/g, "$& ").split(" ").reverse().join(" ");}))
